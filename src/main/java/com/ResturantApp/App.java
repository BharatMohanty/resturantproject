package com.ResturantApp;

import com.ResturantApp.Entity.Resturant;
import com.ResturantApp.Repository.ResturantRepository;

/**
 * Hello world!
 *
 */
public class App  
{
    public static void main( String[] args )
    {
       Resturant resturant = new Resturant();
       resturant.setId(2356L);
       resturant.setName("Rasabali Resturant");
       resturant.setType("Non-veg and Veg");
       resturant.setPrice(100D);
       resturant.setRating(5L);
       resturant.setCity("Bangalore");
       resturant.setContactnumber(7982142245L);
       
       ResturantRepository resturantRepository = new ResturantRepository();
       resturantRepository.saveResturantDetails(resturant);
    }
}

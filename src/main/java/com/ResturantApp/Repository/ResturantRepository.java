package com.ResturantApp.Repository;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.ResturantApp.Entity.Resturant;

public class ResturantRepository {
  public void saveResturantDetails(Resturant resturant) {
	  try {
		  Configuration cfg = new Configuration();
		  cfg.configure();
		  SessionFactory sessionFactory = cfg.buildSessionFactory();
		  Session session = sessionFactory.openSession();
		  Transaction transaction = session.beginTransaction();
		  session.save(resturant);
		  transaction.commit();
		
	} catch (HibernateException e) {
		// TODO: handle exception
	}
  }
}
